﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ITI.ApiScaffolder
{
    internal class ControllerWriter
    {
        private static string GetClassStart(string controllerName)
        {
            var s = "namespace Website.Controllers\n";
            s += "{\n";
            s += "[Route(\"api/[Controller]/[Action]\")]\n";
            s += $"public class {controllerName} : ControllerBase\n";
            s += "{\n";
            return s;
        }

        private static string GetClassEnd()
        {
            var s = "}\n";
            s += "}\n";
            return s;
        }

        private sealed class GetConstructorResult
        {
            public string Result { get; set; }
            public string InterfaceMemberName { get; set; }
        }

        private static GetConstructorResult GetConstructor(string controllerName, string interfaceName)
        {
            var interfaceArgumentName = VarUtil.ToLocalVariableCase(interfaceName.Substring(1));
            var interfaceMemberName = "_" + interfaceArgumentName;

            var s = $"private readonly {interfaceName} {interfaceMemberName};\n";
            s += "\n";
            s += $"public {controllerName}({interfaceName} {interfaceArgumentName})\n";
            s += "{\n";
            s += $"{interfaceMemberName} = {interfaceArgumentName};\n";
            s += "}\n";

            return new GetConstructorResult
            {
                Result = s,
                InterfaceMemberName = interfaceMemberName
            };
        }

        private static string GetMethodStart(MethodModel method, string argumentsString)
        {
            var s = $"public {VarUtil.GetCsTypeName(method.ReturnType)} {method.Name.Replace("Async", "")}({argumentsString})\n";
            s += "{\n";
            return s;
        }

        private static string GetHttpPostMethod(MethodModel method, string interfaceMemberName)
        {
            var requestBodyTypeName = method.Name + "RequestBody";
            var apiArguments = method.Parameters.Select(p => $"public {VarUtil.GetCsTypeName(p.ParameterType)} {VarUtil.ToMemberCase(p.Name)} {{ get; set; }}\n");

            var s = $"public class {requestBodyTypeName} {{\n";
            s += string.Join("", apiArguments);
            s += "}\n\n";

            s += "[HttpPost]\n";
            s += GetMethodStart(method, $"[FromBody] {requestBodyTypeName} body");

            var appServiceArguments = method.Parameters.Select(t => $"body.{VarUtil.ToMemberCase(t.Name)}");

            if (method.ReturnType != typeof(void))
                s += "return ";

            s += $"{interfaceMemberName}.{method.Name}({string.Join(", ", appServiceArguments)});\n";

            s += "}\n";
            return s;
        }

        private static string GetHttpGetMethod(MethodModel method, string interfaceMemberName)
        {
            var apiArguments = method.Parameters.Select(p =>
            {
                var typeName = VarUtil.GetCsTypeName(p.ParameterType);

                if (new Regex(@"\S+Id$").IsMatch(typeName))
                {
                    // Web API does not automatically map guid parameters to instances of our strongly-typed Id classes
                    typeName = "Guid";
                }

                return $"{typeName} {p.Name}";
            });

            var s = "[HttpGet]\n";
            s += GetMethodStart(method, string.Join(", ", apiArguments));

            var appServiceArguments = method.Parameters.Select(t => VarUtil.ToLocalVariableCase(t.Name));
            s += $"return {interfaceMemberName}.{method.Name}({string.Join(", ", appServiceArguments)});\n";

            s += "}\n";
            return s;
        }

        private static string GetMethod(MethodModel method, string interfaceMemberName)
        {
            switch (method.HttpMethod)
            {
                case HttpMethod.Get:
                    return GetHttpGetMethod(method, interfaceMemberName);
                case HttpMethod.Post:
                    return GetHttpPostMethod(method, interfaceMemberName);
                default:
                    throw new ArgumentOutOfRangeException(nameof(method));
            }
        }

        // Returns controller name
        public static string Write(AppInterfaceModel model, StringBuilder sb)
        {
            var controllerName = $"{model.EntityName}Controller";

            // Not generating any using statements - just use ReSharper to add them all
            sb.Append(GetClassStart(controllerName));

            var constructorResult = GetConstructor(controllerName, model.InterfaceName);
            var interfaceMemberName = constructorResult.InterfaceMemberName;
            sb.Append(constructorResult.Result);
            sb.AppendLine();

            foreach (var method in model.MethodModels)
            {
                sb.Append(GetMethod(method, interfaceMemberName));
                sb.AppendLine();
            }

            sb.Append(GetClassEnd());

            return controllerName;
        }

        // Returns file path
        public static string Write(AppInterfaceModel model, string outputDir)
        {
            var sb = new StringBuilder();
            var controllerName = Write(model, sb);

            var path = Path.Combine(outputDir, controllerName + ".cs");
            File.WriteAllText(path, sb.ToString());

            return path;
        }
    }
}