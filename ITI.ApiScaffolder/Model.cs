﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace ITI.ApiScaffolder
{
    internal class AppInterfaceModel
    {
        public string InterfaceName { get; set; }
        public string EntityName { get; set; } // for IUserAppService, EntityName = "User"
        public List<MethodModel> MethodModels { get; set; }
    }

    internal enum HttpMethod
    {
        Get,
        Post
    }

    internal class MethodModel
    {
        public string Name { get; set; }
        public HttpMethod HttpMethod { get; set; }

        public List<ParameterInfo> Parameters { get; set; }
        public Type ReturnType { get; set; }
    }
}