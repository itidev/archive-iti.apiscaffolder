﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ITI.ApiScaffolder
{
    internal class AppServiceReader
    {
        private static MethodModel ReadMethod(MethodInfo method)
        {
            return new MethodModel
            {
                Name = method.Name,
                Parameters = method.GetParameters().ToList(),
                ReturnType = method.ReturnType
            };
        }

        private static List<MethodModel> ReadMethods(Type appInterface)
        {
            return appInterface.GetMethods().Select(ReadMethod).ToList();
        }

        private static string GetEntityName(Type appInterface)
        {
            var name = appInterface.Name;
            name = name.Substring(1); // remove leading I
            name = name.Replace("AppService", "");

            return name;
        }

        public static AppInterfaceModel Read(string interfaceName)
        {
            var assembly = Assembly.Load("AppInterfaces");
            var types = assembly.GetTypes();

            var appInterface = types.SingleOrDefault(t => t.Name.Equals(interfaceName, StringComparison.OrdinalIgnoreCase));
            if (appInterface == null) throw new Exception($"Could not find interface {interfaceName}.");

            var entityName = GetEntityName(appInterface);

            var methodModels = ReadMethods(appInterface);

            return new AppInterfaceModel
            {
                InterfaceName = appInterface.Name,
                EntityName = entityName,
                MethodModels = methodModels
            };
        }
    }
}