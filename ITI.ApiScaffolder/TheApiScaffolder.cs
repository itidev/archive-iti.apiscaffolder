﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ITI.ApiScaffolder
{
    internal class TheApiScaffolder
    {
        public static void AskForHttpMethod(MethodModel method)
        {
            var paramaterNames = method.Parameters.Select(p => p.Name);
            var signature = VarUtil.GetCsTypeName(method.ReturnType) + " ";
            signature += method.Name + "(" + string.Join(", ", paramaterNames) + ")";

            Console.Write($"{signature}: ");

            var keyChar = Console.ReadKey().KeyChar;
            keyChar = char.ToLowerInvariant(keyChar);

            Console.WriteLine();

            switch (keyChar)
            {
                case 'g':
                    method.HttpMethod = HttpMethod.Get;
                    break;
                case 'p':
                    method.HttpMethod = HttpMethod.Post;
                    break;
                default:
                    Console.WriteLine("Invalid input. Try again.");
                    AskForHttpMethod(method);
                    break;
            }
        }

        public static void AskForHttpMethods(List<MethodModel> methods)
        {
            Console.WriteLine("You must select GET or POST for each of the following methods.");
            Console.WriteLine("Type g for GET or p for POST.\n");

            foreach (var method in methods)
            {
                AskForHttpMethod(method);
            }

            Console.WriteLine();
        }

        public static void Write(AppInterfaceModel model)
        {
            // Output is written to folders in bin\Debug
            var outputDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ScaffoldApiOutput");
            Directory.CreateDirectory(outputDir);

            string controllerPath = ControllerWriter.Write(model, outputDir);
            var typeScriptPath = TypeScriptWriter.Write(model, outputDir);

            Console.WriteLine($"Wrote {Path.GetFileName(controllerPath)} and {Path.GetFileName(typeScriptPath)}.");
            Console.WriteLine();
        }

        public void Run(string interfaceName)
        {
            var model = AppServiceReader.Read(interfaceName);
            AskForHttpMethods(model.MethodModels);
            Write(model);
        }
    }
}