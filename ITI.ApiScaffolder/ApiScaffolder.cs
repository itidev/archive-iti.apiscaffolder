﻿using Autofac;
using CommandLine;

namespace ITI.ApiScaffolder
{
    public class ApiScaffolder
    {
        public class Options
        {
            [Value(0)]
            public string AppServiceInterfaceName { get; set; }
        }

        public ApiScaffolder(Options options)
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<TheApiScaffolder>();
            builder.RegisterType<AppServiceReader>();
            builder.RegisterType<ControllerWriter>();
            builder.RegisterType<TypeScriptWriter>();

            var container = builder.Build();

            var apiScaffolder = container.Resolve<TheApiScaffolder>();
            apiScaffolder.Run(options.AppServiceInterfaceName);
        }
    }
}
