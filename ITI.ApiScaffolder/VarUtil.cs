﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ITI.ApiScaffolder
{
    internal static class VarUtil
    {
        public static string ToMemberCase(string typeName)
        {
            var chars = typeName.ToCharArray();
            chars[0] = char.ToUpperInvariant(chars[0]);
            return new string(chars);
        }

        public static string ToLocalVariableCase(string typeName)
        {
            var chars = typeName.ToCharArray();
            chars[0] = char.ToLowerInvariant(chars[0]);
            return new string(chars);
        }

        public static string GetCsTypeName(Type type)
        {
            var typeName = type.Name;

            var rewrites = new Dictionary<string, string>
            {
                ["String"] = "string",
                ["Boolean"] = "bool",
                ["Int32"] = "int",
                ["Int64"] = "long"
            };

            if (rewrites.ContainsKey(typeName))
            {
                typeName = rewrites[typeName];
            }

            if (type.IsGenericType)
            {
                var genericArgumentNames = type.GetGenericArguments().Select(GetCsTypeName);

                typeName = typeName.Split('`')[0];
                typeName += "<" + string.Join(", ", genericArgumentNames) + ">";
            }

            return typeName;
        }
    }
}