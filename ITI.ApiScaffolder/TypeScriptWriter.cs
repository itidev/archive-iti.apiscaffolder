﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ITI.ApiScaffolder
{
    internal class TypeScriptWriter
    {
        private static string GetTypeName(Type type)
        {
            var typeName = type.Name;

            var rewrites = new Dictionary<string, string>
            {
                ["Void"] = "void",
                ["String"] = "string",
                ["Boolean"] = "boolean",
                ["Int32"] = "number",
                ["Int64"] = "number",
                ["DateTime"] = "moment.Moment",
                ["DateTimeOffset"] = "moment.Moment",
            };

            if (rewrites.ContainsKey(typeName))
            {
                typeName = rewrites[typeName];
            }

            if (type.FullName != null && type.FullName.Contains("System.Threading.Task"))
            {
                var genericArgument = type.GetGenericArguments().SingleOrDefault();

                if (genericArgument != null)
                {
                    typeName = GetTypeName(genericArgument);
                } else
                {
                    typeName = "void";
                }
            }
            else if (type.FullName != null && type.FullName.Contains("System.Collections.Generic.List"))
            {
                var genericArgumentTypeName = GetTypeName(type.GetGenericArguments().Single());
                typeName = genericArgumentTypeName + "[]";
            }
            else if (type.Name.Contains("Nullable"))
            {
                var genericArgumentTypeName = GetTypeName(type.GetGenericArguments().Single());
                typeName = genericArgumentTypeName;
            }
            else if (type.IsGenericType)
            {
                var genericArgumentNames = type.GetGenericArguments().Select(GetTypeName);

                typeName = typeName.Split('`')[0];
                typeName += "<" + string.Join(", ", genericArgumentNames) + ">";
            }

            return typeName;
        }

        private static List<Type> GetAllTypes(Type type)
        {
            var types = new List<Type> { type };

            foreach (var genericArg in type.GetGenericArguments())
            {
                types.AddRange(GetAllTypes(genericArg));
            }

            return types;
        }

        private static List<Type> GetAllTypesForMethod(MethodModel method)
        {
            return method.Parameters.SelectMany(p => GetAllTypes(p.ParameterType))
                .Concat(GetAllTypes(method.ReturnType))
                .ToList();
        }

        // If we see a type we don't recognize, import it from Models.
        // This should be correct the vast majority of the time.
        private static string GetModelImports(List<MethodModel> models)
        {
            var allTypes = models.SelectMany(GetAllTypesForMethod);

            var unknownTypes = allTypes.Where(t =>
            {
                var known = new List<string>
                {
                    "string",
                    "boolean",
                    "number",
                    "void",
                    "moment.Moment"
                };

                // Remove ? from nullable types
                var tName = GetTypeName(t).Replace("?", "");
                return !known.Contains(tName);
            });

            var names = unknownTypes
                .Select(t =>
                {
                    var typeName = GetTypeName(t);
                    typeName = typeName.Replace("[]", "");

                    if (typeName.Contains('<'))
                    {
                        typeName = typeName.Substring(0, typeName.IndexOf('<'));
                    }

                    return typeName + ",\n";
                })
                .Distinct()
                .OrderBy(n => n);

            return string.Join("", names);
        }

        private static string GetImports(List<MethodModel> methods)
        {
            var s = "import {\n";
            s += GetModelImports(methods);
            s += "} from 'Models'\n";
            return s;
        }

        private static string GetCallType(MethodModel method)
        {
            var genericParam = "<" + GetTypeName(method.ReturnType) + ">";

            return method.HttpMethod switch
            {
                HttpMethod.Get => "get" + genericParam,
                HttpMethod.Post => "post" + genericParam,
                _ => throw new ArgumentOutOfRangeException(nameof(method)),
            };
        }

        private static string GetUrl(MethodModel method, string entityName)
        {
            entityName = VarUtil.ToLocalVariableCase(entityName);
            var methodName = VarUtil.ToLocalVariableCase(method.Name);

            return $"api/{entityName}/{methodName.Replace("Async", "")}";
        }

        private static string GetMethod(MethodModel method, string entityName)
        {
            var arguments = method.Parameters.Select(p =>
            {
                var name = VarUtil.ToLocalVariableCase(p.Name);
                var type = GetTypeName(p.ParameterType);

                var orUndefined = "";
                if (p.ParameterType.Name.Contains("Nullable"))
                    orUndefined = " | orUndefined";

                return $"{name}: {type} {orUndefined}\n";
            });

            var s = VarUtil.ToLocalVariableCase(method.Name.Replace("Async", ""));
            s += ": (options: {\n";
            s += string.Join("", arguments);
            s += "}) => ";

            s += GetCallType(method);
            s += $"('{GetUrl(method, entityName)}', options),\n";
            return s;
        }

        private static void Write(AppInterfaceModel model, StringBuilder sb)
        {
            sb.Append(GetImports(model.MethodModels));
            sb.AppendLine();

            sb.AppendLine($"export function {VarUtil.ToLocalVariableCase(model.EntityName)}Api({{get, post}}: ApiMethods) {{");
            sb.AppendLine("return {");

            foreach (var method in model.MethodModels)
            {
                sb.Append(GetMethod(method, model.EntityName));
                sb.AppendLine();
            }

            sb.AppendLine("}");
            sb.AppendLine("}");
        }

        // Returns file path
        public static string Write(AppInterfaceModel model, string outputDir)
        {
            var sb = new StringBuilder();
            Write(model, sb);

            var path = Path.Combine(outputDir, model.EntityName + "Api.ts");
            File.WriteAllText(path, sb.ToString());

            return path;
        }
    }
}