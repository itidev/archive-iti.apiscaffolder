﻿using ITI.ApiScaffolder;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Reflection;

namespace UnitTests
{
    [TestClass]
    public class TypeScriptWriterTests
    {
        [TestMethod]
        public void ItWritesFilePath()
        {
            var appInterfaceModel = new AppInterfaceModel
            {
                InterfaceName = "IUserAppService",
                EntityName = "User",
                MethodModels = new List<MethodModel>() {
            new MethodModel {
              Name = "getUsers", HttpMethod = 0, Parameters = new List < ParameterInfo > () {}, ReturnType = typeof (string)
            }
          }
            };
            var result = TypeScriptWriter.Write(appInterfaceModel, "./");
            Assert.AreEqual("./UserApi.ts", result);
        }
    }
}