using Microsoft.VisualStudio.TestTools.UnitTesting;
using ITI.ApiScaffolder;
using System;

namespace UnitTests
{
    [TestClass]
    public class VarUtilTests
    {
        [TestMethod]
        public void ItConvertsToMemberCase()
        {
            var original = "test";
            var result = VarUtil.ToMemberCase(original);
            Assert.AreEqual("Test", result);
        }

        [TestMethod]
        public void ItConvertsToLocalVariableCase()
        {
            var original = "Test";
            var result = VarUtil.ToLocalVariableCase(original);
            Assert.AreEqual("test", result);
        }

        [TestMethod]
        public void ItGetsCsTypeName()
        {
            Type type = typeof(int);
            var result = VarUtil.GetCsTypeName(type);
            Assert.AreEqual("int", result);
        }
    }
}
