# ITI.ApiScaffolder

Generates C# controllers and TypeScript API definitions based on C# app services.

Example usage:

```
    public class ApiScaffolder
    {
        public class Options
        {
            [Value(0)]
            public string AppServiceInterfaceName { get; set; }
        }

        public ApiScaffolder(Options options)
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<TheApiScaffolder>();
            builder.RegisterType<AppServiceReader>();
            builder.RegisterType<ControllerWriter>();
            builder.RegisterType<TypeScriptWriter>();

            var container = builder.Build();

            var apiScaffolder = container.Resolve<TheApiScaffolder>();
            apiScaffolder.Run(options.AppServiceInterfaceName);
        }
    }
```

## Publishing

0. Install https://github.com/microsoft/artifacts-credprovider
1. Update the version number the `ITI.ApiScaffolder.csproj` file.
2. In PowerShell run

   ```pwsh
   ./Update-NuGetPackages.ps1 x.x.x
   ```

   where `x.x.x` is the version number.
